require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  return 0 if arr.empty?
  arr.reduce(:+)
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def in_all_strings?(long_strings, substring)
  long_strings.all? { |string| string.include?(substring) }
end

# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  hash = Hash.new(0)
  answer = []
  string.split.each { |word| word.chars { |char| hash[char] += 1 } }
  hash.each { |x,y| answer << x if y > 1 }
  answer.sort
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  answer1 = ''
  answer2 = ''
  string.split.each do |word|
    if word.length > answer1.length
      answer2 = answer1 if answer1.length > answer2.length
      answer1 = word
    elsif word.length > answer2.length
      answer1 = answer2 if answer2.length > answer1.length
      answer2 = word
    end
  end
  [answer1, answer2].sort
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  alpha = ('a'..'z').to_a
  answer = []
  alpha.each { |char| answer << char if !string.include?(char) }
  answer
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  list = (first_yr..last_yr).to_a
  answer = []
  list.each { |year| answer << year if not_repeat_year?(year) }
  answer
end

def not_repeat_year?(year)
  year.to_s.chars.uniq == year.to_s.chars
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
def one_week_wonders(songs)
  repeated = []
  answer = []
  songs.each_with_index { |song, idx| repeated << song if songs[idx - 1] == song }
  songs.each { |song| answer << song if !repeated.include?(song) }
  answer.uniq
end

def no_repeats?(song_name, songs)

end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
  answer = ''
  place = nil
  string.split.each do |word|
    reversed = word.chars.reverse
    reversed = delete_puncs(reversed)
    reversed.each_with_index do |char, idx|
      if char == 'c' && (place.nil? || idx < place)
        answer = reversed.reverse.join
        place = idx
      end
    end
  end
  answer
end

def delete_puncs(arr)
  alpha = ('a'..'z').to_a
  new_arr = []
  arr.each { |char| new_arr << char if alpha.include?(char) }
  new_arr
end

def c_distance(word)
end

# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

def repeated_number_ranges(arr)
  answer = []
  holder = []
  arr.each_with_index do |num, idx|
    if holder.empty? && num == arr[idx + 1]
      holder << idx
    elsif !holder.empty? && num != arr[idx + 1]
      holder << idx
      answer << holder
      holder = []
    end
  end
  answer
end
